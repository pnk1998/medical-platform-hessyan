package ro.tuc.ds2020.Frontend;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GUImedPlan extends JPanel {

    JTable table;
    JFrame frame;
    Calendar calendar;
    SimpleDateFormat timeFormat;
    String time;
    JLabel timeLabel;

    public GUImedPlan(String[] columnNames, String[][] data) throws InterruptedException {
        frame = new JFrame();
        JButton butonDownload = new JButton("Download Medical Plan");

        timeFormat = new SimpleDateFormat("hh:mm:ss a");
        timeLabel = new JLabel();
        setTime();


        table = new JTable(data,columnNames)
        {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            public Component prepareRenderer(TableCellRenderer r,int row, int column){
                Component c = super.prepareRenderer(r,row,column);

                if(row % 2 == 0)
                    c.setBackground(Color.WHITE);
                else
                    c.setBackground(Color.LIGHT_GRAY);

                if(isCellSelected(row,column))
                    c.setBackground(Color.ORANGE);

                return c;
            }
        };
        table.setPreferredScrollableViewportSize(new Dimension(700,100));
        table.setFillsViewportHeight(true);

        JScrollPane jps = new JScrollPane(table);
        add(jps);

        JPanel panelButton = new JPanel();
        panelButton.add(butonDownload);

        frame.setTitle("Medication plan records");
        frame.setSize(600,250);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(panelButton,BorderLayout.CENTER);
        frame.add(jps,BorderLayout.NORTH);
        frame.add(timeLabel);


    }

    public void setTime() throws InterruptedException {

        while(true) {
            time = timeFormat.format(Calendar.getInstance().getTime());
            timeLabel.setText(time);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
