package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.Frontend.GUImedPlan;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
@Configuration
@EnableScheduling
public class HessianApplication extends JPanel implements ActionListener{

    private static JTable table;
    private static JButton butonDownload;
    private static JFrame frame;
    private static String[][] data;
    private static List<MedPlanDTO> medPlans;
    private static String time;
    private static JLabel timeLabel;

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/hellohessian");
        invoker.setServiceInterface(HelloWorld.class);
        return invoker;
    }

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        ConfigurableApplicationContext context =  SpringApplication.run(HessianApplication.class, args);
        System.out.println("========Client Side===============");
        HelloWorld helloWorld =     context.getBean(HelloWorld.class);
        System.out.println(helloWorld.sayHelloWithHessian("Sal pace-n suflet"));

        medPlans = helloWorld.getMedPlansToday();

        String[] columnNames = {"MedPlan ID", "Start period", "End period","Start hour","End hour","Medicine"};
        data = new String[medPlans.size()*3][6];

        int i=0;

        for(MedPlanDTO mp : medPlans){
            int j=0;
            String[] populate = new String[6];
            String[] populate1 = new String[6];
            String[] populate2 = new String[6];

                populate[0] = mp.getId().toString();
                populate[1] = mp.getStart_period().toString();
                populate[2] = mp.getEnd_period().toString();
                populate[3] = mp.getStart_hour().get(j).toString();
                populate[4] = mp.getEnd_hour().get(j).toString();
                populate[5] = mp.getMedicationList().get(0).getName();

                data[i] = populate;
                j++;
                i++;

                populate1[0] = mp.getId().toString();
                populate1[1] = mp.getStart_period().toString();
                populate1[2] = mp.getEnd_period().toString();
                populate1[3] = mp.getStart_hour().get(j).toString();
                populate1[4] = mp.getEnd_hour().get(j).toString();
                populate1[5] = mp.getMedicationList().get(0).getName();

                data[i] = populate1;
                i++;
                j++;

                populate2[0] = mp.getId().toString();
                populate2[1] = mp.getStart_period().toString();
                populate2[2] = mp.getEnd_period().toString();
                populate2[3] = mp.getStart_hour().get(j).toString();
                populate2[4] = mp.getEnd_hour().get(j).toString();
                populate2[5] = mp.getMedicationList().get(0).getName();

                data[i] = populate2;
                j++;
                i++;
        }

        System.setProperty("java.awt.headless", "false");
        frame = new JFrame();
        JButton butonDownload = new JButton("Download Medical Plan");

        timeLabel = new JLabel();

        table = new JTable(data,columnNames)
        {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            public Component prepareRenderer(TableCellRenderer r, int row, int column){
                Component c = super.prepareRenderer(r,row,column);

                if(row % 2 == 0)
                    c.setBackground(Color.WHITE);
                else
                    c.setBackground(Color.LIGHT_GRAY);

                if(isCellSelected(row,column))
                    c.setBackground(Color.ORANGE);

                return c;
            }
        };
        table.setPreferredScrollableViewportSize(new Dimension(700,100));
        table.setFillsViewportHeight(true);

        JScrollPane jps = new JScrollPane(table);

        JPanel panelButton = new JPanel();

        panelButton.add(butonDownload);

        frame.setTitle("Medication plan records");
        frame.setSize(600,250);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(jps,BorderLayout.NORTH);
        frame.add(panelButton,BorderLayout.CENTER);
        frame.add(timeLabel, BorderLayout.AFTER_LAST_LINE);

        butonDownload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileWriter myWriter = new FileWriter("filename.txt");
                    for(MedPlanDTO mp : medPlans) {
                        myWriter.write(mp.toString()+"\n");
                    }
                    myWriter.close();
                    System.out.println("Successfully wrote to the file.");
                } catch (IOException exc) {
                    System.out.println("An error occurred.");
                    exc.printStackTrace();
                }
            }
        });

        try {
            setTime();
        } catch (InterruptedException e){
            e.printStackTrace();
        }

    }

    @Scheduled(fixedDelay = 86400000)
    public void WriteToFileAfter24Hours(){
        if (medPlans != null) {

            try {
                FileWriter myWriter = new FileWriter("filename.txt");


                for (MedPlanDTO mp : medPlans) {

                    ZonedDateTime t = ZonedDateTime.now(ZoneId.of("Europe/Bucharest"));
                    ZonedDateTime t1 = ZonedDateTime.ofInstant(mp.getStart_period().toInstant(), ZoneId.systemDefault());
                    ZonedDateTime t2 = ZonedDateTime.ofInstant(mp.getEnd_period().toInstant(), ZoneId.systemDefault());

                    if (t.isAfter(t1) && t.isBefore(t2)) {
                        myWriter.write(mp.toString() + "\n");
                    }
                }
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException exc) {
                System.out.println("An error occurred.");
                exc.printStackTrace();
            }
        }
    }

    public static void setTime() throws InterruptedException {

        while(true) {
            ZonedDateTime t = ZonedDateTime.now(ZoneId.of("Europe/Bucharest"));
            time = t.toString().subSequence(0,19).toString().replace("T"," ");
            timeLabel.setText(time);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
